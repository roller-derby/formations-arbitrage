# Syllabus des formations à l'arbitrage

Documents décrivant le programme des formations à l'arbitrage menées au sein du RDT.

## Liste des syllabus

- initiation : Formation initialement menée en octobre 2023 sur une journée, conçue pour des 
débutant.e.s en derby.
- recrutement_estival : Formation en 8 semaines aux règles du roller derby pour les débutant.e.s.
